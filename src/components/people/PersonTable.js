import React, {Component} from 'react';
import PersonRow from './PersonRow';

const tableStyle = {
    width: '100%'
}

export default class PersonTable extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <>
                <table style={tableStyle}>
                    {/*Cabecera de la tabla*/}
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Teléfono</th>
                            <th>Email</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    {/*Cuerpo de la tabla*/}
                    <tbody>
                        {/*Iterar el arreglo people que se encuentra en los props*/}
                        {
                            (this.props.people.length > 0) ?
                                this.props.people.map(
                                    objPerson=>{
                                        return(
                                            <PersonRow setPersonUpdate={this.props.setPersonUpdate} deletePerson={this.props.deletePerson} data={objPerson} key={objPerson.email}/>
                                        );
                                    }
                                )
                            : 
                            <tr>
                                <td colSpan="4">No hay datos</td>
                            </tr>
                        }
                    </tbody>
                </table>
            </>
        );
    }
}