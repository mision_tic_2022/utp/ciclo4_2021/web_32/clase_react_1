import React, {Component} from 'react';
import PersonForm from './PersonForm';
import PersonTable from './PersonTable';

/*
Ejercicio:
Transforme la constante 'people' en un estado 
del componente PersonApp
*/
//const people = []

//Componente de clase
class PersonApp extends Component{

    constructor(props){
        super(props);
        this.state = {
            people: [],
            personUpdate: {id: "", name: "", lastname: "", phone: "", email: ""}
        }
        this.addPerson = this.addPerson.bind(this);
        this.deletePerson = this.deletePerson.bind(this);
        this.setPersonUpdate = this.setPersonUpdate.bind(this);
        this.updatePerson = this.updatePerson.bind(this);
    }

    addPerson(objPerson){
        this.setState({
            people: [...this.state.people, {id: objPerson.email, ...objPerson}]
        });
    }

    deletePerson(email){
        let people = this.state.people;
        let tempo = people.filter(objPerson => (objPerson.email != email));
        this.setState({
            people: tempo
        });
    }

    updatePerson(objPerson){        
        let people = this.state.people;
        people.map(element => {
            if(element.id == objPerson.id){
                element.name = objPerson.name;
                element.lastname = objPerson.lastname;
                element.phone = objPerson.phone;
                element.email = objPerson.email;
            }
        });
        //console.log(people);
        
        this.setState({
            people: people
        })
        //console.log("Estado desde PersonApp-> ", this.state.people);
    }

    setPersonUpdate(objPerson){
        this.setState({
            personUpdate: {id: objPerson.email, ...objPerson}
        })
    }

    render(){
        return(
            //Etiqueta Fragment, es una etiqueta vacía, al momento de renderizar se elimina
            <>
                <h1>Registro Personas - Componentes de Clases</h1>
                <PersonForm updatePerson={this.updatePerson} personUpdate={this.state.personUpdate} addPerson={this.addPerson}/>
                <PersonTable setPersonUpdate={this.setPersonUpdate} deletePerson={this.deletePerson} people={this.state.people}/>
            </>
        );
    }

}

export default PersonApp;