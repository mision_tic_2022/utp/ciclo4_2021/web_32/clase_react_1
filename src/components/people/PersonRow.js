/*************
 * Luis Eduardo
 **************/
import React, {Component} from "react";

export default class PersonRow extends Component {
  constructor(props) {
    super(props);
    this.btnDelete = this.btnDelete.bind(this);
    this.btnEdit = this.btnEdit.bind(this);
  }

  btnDelete(e){
    this.props.deletePerson(this.props.data.email);
  }

  btnEdit(e){
    this.props.setPersonUpdate(this.props.data);
    //console.log("Persona a editar-> ", this.props.data);
  }

  render() {
    return (
        <>
          <tr>
            <td>{this.props.data.name}</td>
            <td>{this.props.data.lastname}</td>
            <td>{this.props.data.phone}</td>
            <td>{this.props.data.email}</td>
            <td>
              <button onClick={this.btnEdit}>Editar</button> 
              &nbsp;
              <button onClick={this.btnDelete}>Eliminar</button>
            </td>
          </tr>
        </>
    )
  }
}