import React, { Component } from 'react';

const objForm = {
    name: "",
    lastname: "",
    phone: "",
    email: ""
}

export default class PersonForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            form: objForm
        }
        /*
        if (this.props.personUpdate.id != "") {
            this.state = {
                form: this.props.personUpdate
            }
        } else {
            this.state = {
                form: objForm
            }
        }
        */

        //Bindear
        this.handlerValue = this.handlerValue.bind(this);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    //Ciclo de vida
    //Identifica si un atributo/variable del componente se actualiza
    componentDidUpdate(prevProps, prevState){
        // console.log(prevProps);
        // console.log("Hubo una actualización");
        // console.log(this.props.personUpdate);
        if(this.props.personUpdate.id != prevProps.personUpdate.id){
            this.setState({
                form: this.props.personUpdate
            });
        }
    }


    //Manejador de eventos
    handlerValue(e) {
        //Capturar el estado actual del objeto form
        let form = this.state.form;
        //Actualizar/setear el estado
        this.setState({
            form: { ...form, [e.target.name]: e.target.value }
        });
    }

    handlerSubmit(e) {
        e.preventDefault();
        this.setState({
            form: objForm
        })
        let objPerson = this.state.form;
        this.props.addPerson(objPerson);
        
    }

    handleUpdate(e){
        console.log("Actualizar-> ", this.state.form);
        this.props.updatePerson(this.state.form);
    }

    render() {
        return (
            <>
                <form onSubmit={this.handlerSubmit}>
                    {/*Nombre*/}
                    <label htmlFor="name">Nombre: </label>
                    <input name="name" id="name" type="text"
                        value={this.state.form.name} onChange={this.handlerValue}
                        placeholder="Escribe tu nombre..." />
                    {/*Apellido*/}
                    <label htmlFor="lastname">Apellido: </label>
                    <input name="lastname" id="lastname" type="text"
                        value={this.state.form.lastname} onChange={this.handlerValue}
                        placeholder="Escribe tu apellido..." />
                    <br />
                    <br />
                    {/*Teléfono*/}
                    <label htmlFor="phone">Teléfono: </label>
                    <input name="phone" id="phone" type="tel"
                        value={this.state.form.phone} onChange={this.handlerValue}
                        placeholder="Escribe tu teléfono..." />
                    {/*Email*/}
                    <label htmlFor="email">Email: </label>
                    <input name="email" id="email" type="email"
                        value={this.state.form.email} onChange={this.handlerValue}
                        placeholder="Escribe tu email..." />
                    <br />
                    <br />
                    <button type="submit" id="btnRegistrar">Registrar</button>
                    &nbsp;
                    <button type="button" onClick={this.handleUpdate} id="BtnActualizar">Actualizar</button>
                </form>
            </>
        );
    }

}