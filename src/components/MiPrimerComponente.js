import React, {Component} from 'react';

const nombres = ["Juan", "Pedro", "Ana", "Juliana"];

const estilo = {color: 'green'}
/******************************
Componente basado en clases
*******************************/
class MiPrimerComponente extends Component{

    //EL constructor es el primer método a ejecutarse 
    //antes de pintar los elementos en el DOM
    /*
    constructor(props){
        super(props);
        console.log("Mi primer componente");
    }
    */

    //Properties initializer
    /*
    estilo = () => { 
        return {color: 'green'}
     }
     */

    render(){
        return(
            <div>
                <h1>Mi Primer Componente</h1>
                <h2>Misión TIC 2022</h2>
                <p style={estilo}>
                    esto es un párrafo
                </p>             
                {nombres}
                <form>
                    <label for="nombre">Nombre: </label>
                    <input name="nombre" type="text"/>
                    <br/>
                    <label for="apellido">Apellido: </label>
                    <input name="apellido" type="text"/>
                    <hr/>
                </form>
            </div>            
        );
    }

}

export default MiPrimerComponente;